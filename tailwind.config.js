/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,js,ts,jsx,tsx}",
  ],
  theme: {
    extend : {
      
      colors : {
        tomato : "#ff6347",

        primary : {
          100 : "#F2F7F7",
          500 : "#9BC1BC",
          900 : "#172626",
        }
      }
    },
  },
  plugins: [],
}

